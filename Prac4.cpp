//==============================================================================
// Copyright (C) John-Philip Taylor
// tyljoh010@myuct.ac.za
//
// This file is part of the EEE4084F Course
//
// This file is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//==============================================================================

#include "Prac4.h"
//------------------------------------------------------------------------------

// The "Hello World" example has been adapted from
// https://en.wikipedia.org/wiki/Message_Passing_Interface#Example_program

void Master() {
	int  j;
	MPI_Status stat;
	printf("%d processes running...\n", numprocs);
	
	char* images_name[] = {"Data/small.jpg", "Data/butterfly.JPG", "Data/fly.jpg", "Data/greatwall.jpg"};
	
	char result_filename[50];
	sprintf(result_filename, "result_image_%d.csv", image_index);
	//printf("%s\n", result_filename);
	boolean exist;
	if( access( result_filename, F_OK ) != -1 ) {
		// file exists
		fp = fopen(result_filename, "a");
		exist = true;
		if (prev_numprocs!=numprocs) fprintf(fp, "\n\n\n");
	}
	else {
		// file doesn't exist
		fp = fopen(result_filename, "w+");
		exist = false;
	}

	if (fp == NULL) {
		fprintf(stderr, "Can't open input file in.list!\n");
		MPI_Abort (MPI_COMM_WORLD,1);
		exit(1);
	}

	if (prev_numprocs!=numprocs) fprintf(fp, "%d processes", numprocs);
	fprintf(fp, "\n");
	
	// Read the input image
	if(!Input.Read(images_name[image_index])) {
		printf("Cannot read image\n");
		return;
	}

	// Allocated RAM for the output image
	if(!Output.Allocate(Input.Width, Input.Height, Input.Components)) return;

	// Do block allocation.............................................
	int blocks[numprocs-1];
	int counter = 0;
	int block_size = Input.Height/(numprocs-1);

	for (int i = 0; i < numprocs-1; i++) {
		if (i != numprocs-2) blocks[i] = block_size + 4;  /* the 4 is meant for overlaping in order to resolve the issue of the 9x9 filter size */
		else blocks[i] = Input.Height-counter;   // handle case of mismatch between numprocs and Input.Height.
		counter += block_size;
	}

	int index = 0;
	
	int image_parameters[2];
	int buffer_size;
	tic();
	for(j = 1; j < numprocs; j++) {
		if (j == 1) {
			image_parameters[0] = blocks[j-1];    // height width.
			image_parameters[1] = Input.Width;
			buffer_size = Input.Width * blocks[j-1] * Input.Components;
		}
		else {
			image_parameters[0] = blocks[j-1] + 4;    // height width.
			image_parameters[1] = Input.Width;
			buffer_size = Input.Width * image_parameters[0] * Input.Components;
		}
		MPI_Send(image_parameters, 2, MPI_INT, j, TAG, MPI_COMM_WORLD);
		MPI_Send(Input.Image + index, buffer_size, MPI_BYTE, j, TAG, MPI_COMM_WORLD);
		index += buffer_size-Input.Width * Input.Components * 8;	// minus 4 rows above and 4 rows below.
	}

	index = 0;
	
	//printf("receiving from slaves....\n");
	
	for(j = 1; j < numprocs; j++) {
		// This is blocking: normally one would use MPI_Iprobe, with MPI_ANY_SOURCE,
		// to check for messages, and only when there is a message, receive it 
		// with MPI_Recv.  This would let the master receive messages from any
		// slave, instead of a specific one only.
		//printf("index is %d...\n", index);
		int buffer_size = (j != numprocs-1) ? Input.Width*Input.Components*(blocks[j-1]-4) : Input.Width*Input.Components*blocks[j-1];
		MPI_Recv(Output.Image+index, buffer_size*Input.Components, MPI_BYTE, j, TAG, MPI_COMM_WORLD, &stat);
		index += buffer_size;
	}

	printf("Time = %lg ms\n", (double)toc()/1e-3);
	fprintf(fp, "%f", (double)toc()/1e-3);

	// Write the output image
	if(!Output.Write("Data/Output.jpg")) {
		printf("Cannot write image\n");
		return;
	}
	fclose(fp);
}
//------------------------------------------------------------------------------

void Slave(int ID) {
	// Start of "Hello World" example..............................................
	int image_parameters[2];   // height, width.
	int buffer_size;
	int start, stop;
	char idstr[32];
	unsigned char * window = new unsigned char[81];
	MPI_Status stat;

	// receive from rank 0 (master): 
	// This is a blocking receive, which is typical for slaves.
	MPI_Recv(image_parameters, 2, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat);

	buffer_size = image_parameters[0] * image_parameters[1];	// height*width.
	start = (ID == 1) ? 0 : 4;
	stop = (ID != numprocs - 1) ? (image_parameters[0] - 4) : image_parameters[0];
	//printf("slave %d has block size %d\n", ID, image_parameters[0]);
	//printf("slave %d processing from %d - %d\n", ID, (ID-1)*76+start, (ID-1)*76+stop);
	
	if(!Input.Allocate(image_parameters[1], image_parameters[0], 3)) return;
	//Input.Allocate(image_parameters[1], image_parameters[0], 3);
	MPI_Recv(Input.Image, buffer_size*Input.Components, MPI_BYTE, 0, TAG, MPI_COMM_WORLD, &stat);

	if(!Output.Allocate(image_parameters[1], stop - start/*image_parameters[0]-4*/, 3/*Input.Components*/)) return;

	for(int y = start; y < stop; y++) {
		for(int x = 0; x < Input.Width; x++) {
			for(int colour = 0; colour < Input.Components; colour++) {
				int i = 0;
				for(int fy = (y - 4); fy <= (y + 4); fy++) {
					for (int fx = (x - 4); fx <= (x + 4) ; fx++) {
						if ((fx >= 0) && (fx < Input.Width) && (fy >= 0) && (fy < Input.Height)) {
							window[i] = Input.Rows[fy][fx * Input.Components + colour];
							i++;
						}
					}
				}
				std::sort(window, window + i);
				Output.Rows[y-start][x * Input.Components + colour] = window[i/2]/*sort(window, i)*/;
				//printf("got here\n");
			}
		}
    }

	// send to rank 0 (master):
	MPI_Send(Output.Image, Input.Width * Input.Components * (stop-start), MPI_BYTE, 0, TAG, MPI_COMM_WORLD);
}

//------------------------------------------------------------------------------

int main(int argc, char** argv) {
	int myid;
	prev_numprocs = 0;
	
	// MPI programs start with MPI_Init
	MPI_Init(&argc, &argv);

	// find out how big the world is 
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

	// and this processes' rank is 
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);

	// At this point, all programs are running equivalently, the rank
	// distinguishes the roles of the programs, with
	// rank 0 often used as the "master". 
	
	for (int i = 0; i < 4; i++) {
		image_index = i;
		
		for (int j = 0; j < 5; j++) {
			if(myid == 0) Master();
			else          Slave (myid);
			prev_numprocs = numprocs;
		}
		prev_numprocs = 0;
	}
	// MPI programs end with MPI_Finalize
	MPI_Finalize();
	return 0;
}
//------------------------------------------------------------------------------
